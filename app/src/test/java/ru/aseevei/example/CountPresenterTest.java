package ru.aseevei.example;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import ru.aseevei.example.project.base.BaseModel;
import ru.aseevei.example.project.ui.count.CountFragment;
import ru.aseevei.example.project.ui.count.CountPresenter;
import static junit.framework.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests for CountPresenter Methods
 */
@RunWith(MockitoJUnitRunner.class)
    public class CountPresenterTest {

    int count = 10;
    int incrment = 2;
    int max = 100;

    @Mock
    CountFragment fragment;
    @Mock
    BaseModel model;
    CountPresenter presenter;

    @Before
    public void setUp() throws Exception {
        when(model.getCount()).thenReturn(count);
        when(model.getIncrement()).thenReturn(incrment);
        when(model.getMax()).thenReturn(max);

        presenter = new CountPresenter(model);
        presenter.bindView(fragment);
    }

    /**
     * Is data loading from Model in Constructor
     */
    @Test
    public void loadDataFromModuleTest() {
        assertEquals(count, presenter.getCount());
        assertEquals(incrment, presenter.getIncrement());
        assertEquals(max, presenter.getMax());
    }

    /**
     * Does Count increase on Increment and data sends to View when onCount
     */
    @Test
    public void onCountTest() {
        presenter.onCount();
        assertEquals(count + incrment, presenter.getCount());
        verify(fragment).setText(String.valueOf(count + incrment));
    }

    /**
     * Was data reset when count >= max
     */
    @Test
    public void resetOnMaxCount() {
        max = 6;
        when(model.getMax()).thenReturn(6);
        presenter = new CountPresenter(model);
        presenter.onCount();
        assertEquals(incrment, presenter.getCount());
    }

    /**
     * Was data sent to View when new View was binded
     */
    @Test
    public void sendDataOnBindTest() {
        verify(fragment).setText(String.valueOf(count));
    }

    /**
     * Does data save onDestroy
     */
    @Test
    public void saveDataOnDestroy() {
        presenter.saveData();
        verify(model).setCount(anyInt());
    }
}