package ru.aseevei.example;

import android.content.pm.ActivityInfo;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v4.app.Fragment;
import static android.support.test.espresso.Espresso.*;
import static android.support.test.espresso.action.ViewActions.*;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static junit.framework.Assert.*;
import static org.junit.Assert.assertNotEquals;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import nl.dionsegijn.steppertouch.StepperTouch;
import ru.aseevei.example.project.MainActivity;
import ru.aseevei.example.project.ui.settings.SettingsFragment;

/**
 * Tests for rotation activity. Check is data and Presenter didn't change after rotate
 */
@RunWith(AndroidJUnit4.class)
public class SettingsFragmentRotateTest {

    private SettingsFragment settingsFragment;
    private StepperTouch stStepper;
    private DiscreteSeekBar sbMax;
    @Rule public ActivityTestRule<MainActivity> mActivityRule =
            new ActivityTestRule<>(MainActivity.class, true, true);

    @Before
    public void setUp() {
        onView(withId(R.id.menu_settings)).perform(click());
        initFragment();
    }

    /**
     * Check if Presenter and Data same after rotate
     */
    @Test
    public void chechDataSaveRotate() {
        int max = 50;
        String presenterLink = settingsFragment.getPresenter().toString();
        settingsFragment.getPresenter().onMax(max);
        mActivityRule.getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        initFragment();
        assertEquals(max, settingsFragment.getPresenter().getMax());
        assertEquals(presenterLink, settingsFragment.getPresenter().toString());
    }

    public void initFragment() {
        Fragment fragment = mActivityRule.getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
        assertNotNull(fragment);
        settingsFragment = (SettingsFragment) fragment;
        stStepper = settingsFragment.getView().findViewById(R.id.stIncrement);
        sbMax = settingsFragment.getView().findViewById(R.id.sbMax);
    }
}