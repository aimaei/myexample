package ru.aseevei.example.project.di.module;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;
import ru.aseevei.example.project.di.annotation.AppContext;
import ru.aseevei.example.project.di.base.FragmentComponentBuilder;
import ru.aseevei.example.project.di.component.CountComponent;
import ru.aseevei.example.project.di.component.SettingsComponent;
import ru.aseevei.example.project.ui.count.CountFragment;
import ru.aseevei.example.project.ui.settings.SettingsFragment;
import ru.aseevei.example.project.utils.Prefs;

/**
 * Provides Context, Prefs, CountFragment/SettingsFragment builders
 */
@Module(subcomponents = {CountComponent.class, SettingsComponent.class})
public class AppModule {

    private final Context context;

    public AppModule(Context context) {
        this.context = context;
    }

    @Provides
    @AppContext
    Context provideContext() {
        return context;
    }

    @Provides
    @Singleton
    Prefs providePrefs(SharedPreferences preferences) {
        return new Prefs(preferences);
    }

    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences(@AppContext Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    @Provides
    @IntoMap
    @ClassKey(CountFragment.class)
    FragmentComponentBuilder provideCountFragmentBuilder(CountComponent.Builder builder) {
        return builder;
    }

    @Provides
    @IntoMap
    @ClassKey(SettingsFragment.class)
    FragmentComponentBuilder provideSettingsFragmentBuilder(SettingsComponent.Builder builder) {
        return builder;
    }
}
