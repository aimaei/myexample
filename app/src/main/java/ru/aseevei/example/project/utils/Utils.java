package ru.aseevei.example.project.utils;

import android.app.Application;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import ru.aseevei.example.R;
import ru.aseevei.example.project.ui.count.CountFragment;

public class Utils {

    /**
     * Creates singletones that needed context like Fabric, Realm, UrbanAirship etc.
     * @param app Application class
     */
    public static void createSingletones(Application app) {
        Fabric.with(app, new Crashlytics());
    }

    /**
     * Open new fragment from another fragmnet with alpha animation
     * @param currentFragment Fragment you call this method from
     * @param newFragment Fragment you will open
     */
    public static void openFragment(Fragment currentFragment, Fragment newFragment) {
        if (currentFragment != null && newFragment != null) {
            currentFragment.getFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.abc_fade_in, R.anim.abc_fade_out)
                    .replace(R.id.container, newFragment).commitAllowingStateLoss();
        }
    }

    /**
     * Open fragment from AppCompatActivity
     * @param activity Activity you call this method from
     * @param newFragment Fragment you will open
     */
    public static void openFragment(AppCompatActivity activity, Fragment newFragment) {
        if (activity != null && newFragment != null) {
            activity.getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.abc_fade_in, R.anim.abc_fade_out)
                    .replace(R.id.container, newFragment).commitAllowingStateLoss();
        }
    }

    /**
     * Finds and returns first fragment, that should be openned on start app
     */
    public static Fragment getFirstFragment() {
        return new CountFragment();
    }
}
