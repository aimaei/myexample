package ru.aseevei.example.project;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import butterknife.ButterKnife;
import ru.aseevei.example.R;
import ru.aseevei.example.project.base.BaseFragment;
import ru.aseevei.example.project.utils.Utils;

/**
 * Provides container and open first fragment
 */
public class MainActivity extends AppCompatActivity {

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        ButterKnife.bind(this);
        openContent();
    }

    void openContent() {
        if (getSupportFragmentManager().findFragmentById(R.id.container) == null)
            Utils.openFragment(this, Utils.getFirstFragment());
    }

    /**
     * Finds open BaseFragment and call onBackPressed method in fragment
     * if onBackPressed return false - finish activity
     */
    @Override public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if (fragment == null || !(fragment instanceof BaseFragment) || !((BaseFragment) fragment).onBackPressed()) {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                break;
        }

        return false;
    }
}
