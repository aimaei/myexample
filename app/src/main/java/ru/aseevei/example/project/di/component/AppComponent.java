package ru.aseevei.example.project.di.component;

import android.content.Context;
import javax.inject.Singleton;
import dagger.Component;
import ru.aseevei.example.project.di.ComponentsHolder;
import ru.aseevei.example.project.di.annotation.AppContext;
import ru.aseevei.example.project.di.module.AppModule;
import ru.aseevei.example.project.utils.Prefs;

/**
 * App Component allow to get Context, Prefs and Inject ComponentHolder
 */
@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {

    void injectComponentsHolder(ComponentsHolder componentsHolder);

    @AppContext
    Context getContext();

    @Singleton
    Prefs getPrefs();
}
