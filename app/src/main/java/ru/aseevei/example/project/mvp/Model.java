package ru.aseevei.example.project.mvp;

public interface Model {

    public int getCount();
    public int getIncrement();
    public int getMax();

    public void setCount(int count);
    public void setIncrement(int increment);
    public void setMax(int max);
}
