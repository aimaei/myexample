package ru.aseevei.example.project.di.module;

import javax.inject.Named;
import dagger.Module;
import dagger.Provides;
import ru.aseevei.example.project.base.BaseModel;
import ru.aseevei.example.project.di.annotation.FragmentContext;
import ru.aseevei.example.project.di.annotation.SettingsContext;
import ru.aseevei.example.project.di.base.FragmentModule;
import ru.aseevei.example.project.ui.settings.SettingsFragment;
import ru.aseevei.example.project.ui.settings.SettingsPresenter;

/**
 * Provides SettingsFragment and SettingsPresenter
 */
@Module
public class SettingsModule implements FragmentModule {

    private final SettingsFragment settingsFragment;

    public SettingsModule(SettingsFragment settingsFragment) {
        this.settingsFragment = settingsFragment;
    }

    @Provides
    @SettingsContext
    SettingsFragment provideSettingsFragment() {
        return settingsFragment;
    }

    @Provides
    @FragmentContext
    @Named("settings")
    SettingsPresenter provideSettingsPresenter(BaseModel baseModel) {
        return new SettingsPresenter(baseModel);
    }
}