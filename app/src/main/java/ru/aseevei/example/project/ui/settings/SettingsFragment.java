package ru.aseevei.example.project.ui.settings;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;
import javax.inject.Inject;
import javax.inject.Named;
import butterknife.BindView;
import butterknife.ButterKnife;
import nl.dionsegijn.steppertouch.StepperTouch;
import ru.aseevei.example.R;
import ru.aseevei.example.project.base.BaseFragment;
import ru.aseevei.example.project.di.DaggerApp;
import ru.aseevei.example.project.di.component.SettingsComponent;
import ru.aseevei.example.project.di.module.SettingsModule;
import ru.aseevei.example.project.ui.count.CountFragment;
import ru.aseevei.example.project.utils.Utils;
import ru.aseevei.example.project.utils.WidgetUtils;

/**
 * Settings allow you to change increment, max value and reset count
 */
public class SettingsFragment extends BaseFragment implements SettingsView {

    //regionData
    SettingsComponent settingsComponent;
    @Inject @Named("settings") SettingsPresenter presenter;
    @BindView(R.id.stIncrement) StepperTouch stIncrement;
    @BindView(R.id.sbMax) DiscreteSeekBar sbMax;
    @BindView(R.id.btReset) Button btReset;
    //endregion

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.settings_fragment, container, false);
        // init views
        setUnbinder(ButterKnife.bind(this, v));
        setSettingsToolbar();
        stIncrement.stepper.setMin(0);
        // dagger
        settingsComponent = (SettingsComponent) DaggerApp.getApp(getContext()).getComponentsHolder()
                .getFragmentComponent(getClass(), new SettingsModule(this));
        settingsComponent.inject(this);
        // actions
        stIncrement.stepper.addStepCallback((i, b) -> presenter.onIncrement(i)); // increment change
        sbMax.setOnProgressChangeListener(WidgetUtils.getSeekListener(i -> presenter.onMax(i))); // max bar change
        btReset.setOnClickListener(view -> {presenter.onReset(); onBackPressed();}); // reset pressed
        //presenter
        setPresenter(presenter);
        presenter.bindView(this);

        return v;
    }

    /**
     * Change Title and show Home Button
     */
    void setSettingsToolbar() {
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getActivity().setTitle(getString(R.string.settings));
    }

    /**
     * Set Stepper Touch Value
     */
    @Override
    public void setIncrement(int increment) {
        stIncrement.stepper.setValue(increment);
    }

    /**
     * Set Seek Bar value and Max for Stepper Touch
     */
    @Override
    public void setMax(int max) {
        stIncrement.stepper.setMax(max);
        sbMax.setProgress(max);
    }

    /**
     * Destroy fragment and open previous fragment
     */
    @Override
    public boolean onBackPressed() {
        Utils.openFragment(this, new CountFragment());
        return true;
    }

    public SettingsPresenter getPresenter() {
        return presenter;
    }
}
