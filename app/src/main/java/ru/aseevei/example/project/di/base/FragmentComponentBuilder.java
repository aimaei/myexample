package ru.aseevei.example.project.di.base;

public interface FragmentComponentBuilder<C extends FragmentComponent, M extends FragmentModule>   {
    C build();
    FragmentComponentBuilder<C,M> module(M module);
}