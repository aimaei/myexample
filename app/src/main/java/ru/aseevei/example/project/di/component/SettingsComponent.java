package ru.aseevei.example.project.di.component;

import dagger.Subcomponent;
import ru.aseevei.example.project.base.BaseModel;
import ru.aseevei.example.project.di.annotation.FragmentContext;
import ru.aseevei.example.project.di.annotation.SettingsContext;
import ru.aseevei.example.project.di.base.FragmentComponent;
import ru.aseevei.example.project.di.base.FragmentComponentBuilder;
import ru.aseevei.example.project.di.module.SettingsModule;
import ru.aseevei.example.project.ui.settings.SettingsFragment;

/**
 * Count Subcomponent allow to get BaseModel and SettingsFragment
 */
@FragmentContext
@Subcomponent(modules = SettingsModule.class)
public interface SettingsComponent extends FragmentComponent<SettingsFragment> {

    @Subcomponent.Builder
    interface Builder extends FragmentComponentBuilder<SettingsComponent, SettingsModule> {}

    @SettingsContext SettingsFragment getSettingsFragment();
    @FragmentContext BaseModel getBaseModel();
}